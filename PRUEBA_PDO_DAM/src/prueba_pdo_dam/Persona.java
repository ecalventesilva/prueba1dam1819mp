package prueba_pdo_dam;

public class Persona {

    String nombre;
    String apellidos;
    int edad;
    float altura;
    String puesto;
    float sueldo;
    Vaca[] misVacas;

    public String imprimePersona() { //SIN STATIC

        return nombre + " : " + this.apellidos + " : " + this.edad + " : " + this.altura; //Es indiferente poner el this

    }

    public String cuantoGana() {
        return "Gana " + this.sueldo + " € ";
    }

    public Float proporcionSueldo(Persona p2) {
        float proporcion = p2.sueldo / sueldo;
        return proporcion;
    }

    public float ajustaSueldoA(Persona per) {
        if (this.puesto.equals("Ordeñar") && per.puesto.equals("Gerencia")) {
            this.sueldo = (float) 0.5 * per.sueldo;
        }

        if (this.puesto.equals("Ordeñar") && per.puesto.equals("Carniceria")) {
            this.sueldo = 1.25f * per.sueldo;
        }

        if (this.puesto.equals("Gerencia")
                && per.puesto.equals("Ordeño")) {
            this.sueldo = 1.5f * per.sueldo;
        }
        if (this.puesto.equals("Gerencia")
                && per.puesto.equals("Carnicería")) {
            this.sueldo = 1.75f * per.sueldo;
        }
        if (this.puesto.equals("Carnicería")
                && per.puesto.equals("Gerencia")) {
            this.sueldo = 0.25f * per.sueldo;
        }
        if (this.puesto.equals("Carnicería")
                && per.puesto.equals("Ordeño")) {
            this.sueldo = 0.75f * per.sueldo;
        }

        return per.sueldo;
    }

    /**
     * Dice si la persona que llama la funcion es mayor de una cierta edad
     *
     * @param int edad la edad con la que vamos a comparar a la persona
     * @return devuelve true si es mayor, false si es menor o de igual edad
     */
    public boolean esMayorDe(int edad) {

        return this.edad > edad;
    }

    public void quedarmeVacas(Vaca[] todas) {
        int cmv=0;
        this.misVacas = new Vaca[todas.length];
        
        for(int i=0;i<todas.length;i++){
        
            if(this.puesto=="Gerencia"){
            
            }else{
                if(this.puesto=="Carnicería"){
                    this.misVacas[cmv]=todas[i];   //cmv=contador mis vacas
                }else{
                
                    this.misVacas[cmv]=todas[i];
                }
                
            }
            cmv++;
            System.out.println(todas[i]);
        }
       
    } 
    
        public void imprimeVacas(Vaca[] misVacas) {
            
            this.quedarmeVacas(misVacas);
            
            
        }

}
