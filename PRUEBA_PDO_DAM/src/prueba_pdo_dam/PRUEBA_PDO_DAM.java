
package prueba_pdo_dam;

import java.util.Scanner;


public class PRUEBA_PDO_DAM {
    /*   
 *por 30xp:
    
-Añadir a la clase Persona un String "puesto". Usarlo solo para valores "ordeño","carniceria" o "gerencia".
-Añadir a la clase Persona un Float "sueldo", que expresará el sueldo en euros.
-Añadir un método cuantoGana a la clase Persona, que devuelva el String "Gana"+sueldo+" €"
-Añadir un método proporcionSueldo a la clase Persona, que reciba una persona por argumentos, y devuelva un float con 
  cuántas veces gana esa persona el sueldo de this.
-Añadir un método ajustaSueldoA, que reciba un objeto Persona, y asigne el sueldo de this, poniéndolo en proporción al 
  del objeto que recibe por parámetro. Un/a ordeñador/a debería ganar un 25% más que alguien de carnicería, y un/a 
    gerente un 50% más que alguien de ordeño.

-Crear la clase Vaca, con Strings para nombre, número de serie y función ("ordeño" o "carniceria"), un Float peso, un int edad, y un array de float con litros ordeñados en el día.
-Crear un método asignaFunción a una vaca, de forma que si pesa más de 200kg, se asigne a carnicería, y si pesa menos, a ordeño.
-Añadir a la clase persona un Array de Vacas
-Crear en la clase persona un método quedarmeVacas(Vaca[] vac), que copie en el array interno a las vacas que corresponden a su puesto de trabajo.*/

    public static void main(String[] args) {

        int a=3;
        Integer b=new Integer(3);
        Scanner sc=new Scanner(System.in);
        Persona p1= new Persona();
        p1.nombre="Eduardo";
        p1.edad=27;
        p1.apellidos="Calvente";
        p1.altura=1.80f;
        p1.sueldo=2000f;
        
        
        Persona p2=new Persona();
        p2.nombre="Lola";
        p2.apellidos="Mento";
        p2.sueldo=1000f;
        
       // Persona[] ap/*(ap=ArrayPersona)*/= new Persona[3];
        
        
       
       
        System.out.println(
                p1.imprimePersona());
        
        System.out.println(
                p1.cuantoGana());
        
          System.out.println(
                p2.imprimePersona());
        
        System.out.println(
                p2.cuantoGana());
        
        System.out.println(p1.proporcionSueldo(p2));
        
       
       Vaca v1=new Vaca();
     v1.nombre="Vaca 1";
     v1.peso=250;
     v1.litros=new float[3];
     v1.asignarFuncion();
     
     Vaca[] todas={v1,v1};
     
     p2.quedarmeVacas(todas);
        
        
}

 
}
