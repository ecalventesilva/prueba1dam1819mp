/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectosourcetree.Poo_AMR;

/**
 *
 * @author 1DAM
 */
public class Prueba_POO {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        
        -Añadir a la clase Persona un String "puesto". Usarlo solo para valores "ordeño","carniceria" o "gerencia".
-Añadir a la clase Persona un Float "sueldo", que expresará el sueldo en euros.
-añadir un método cuantoGana a la clase Persona, que devuelva el String "Gana"+sueldo+" €"
-añadir un método proporcionSueldo a la clase Persona, que reciba una persona por argumentos, y devuelva un float con cuántas veces gana esa persona el sueldo de this.
-Añadir un método ajustaSueldoA, que reciba un objeto Persona, y asigne el sueldo de this, poniéndolo en proporción al del objeto que recibe por parámetro. Un/a ordeñador/a debería ganar un 25% más que alguien de carnicería, y un/a gerente un 50% más que alguien de ordeño.
-Crear la clase Vaca, con Strings para nombre, número de serie y función ("ordeño" o "carniceria"), un Float peso, un int edad, y un array de float con litros ordeñados en el día.
-Crear un método asignaFunción a una vaca, de forma que si pesa más de 200kg, se asigne a carnicería, y si pesa menos, a ordeño.
-Añadir a la clase persona un Array de Vacas
-Crear en la clase persona un método quedarmeVacas(Vaca[] vac), que copie en el array interno a las vacas que corresponden a su puesto de trabajo.
        */
Persona persona1 = new Persona(); 


        persona1.nombre = "Adrian";
        persona1.apellidos = "Moreno";
        persona1.nacionalidad = "Española";
        persona1.edad = 20;
        persona1.altura = 1.71f;
        persona1.puesto = "Gerencia";
          persona1.sueldo = 1000;
          
        Persona persona2 = new Persona();
        persona2.nombre = "Edu";
        persona2.apellidos = "Calvente";
        persona2.nacionalidad = "Ingles";
        persona2.edad = 27;
        persona2.altura = 1.80f;
        persona2.puesto = "Gerencia";
        persona2.sueldo = 2000;
               
         Persona persona3 = new Persona();
          persona3.nombre = "Alex";
        persona3.apellidos = "Cervante";
        persona3.nacionalidad = "Aleman";
        persona3.edad = 27;
        persona3.altura = 1.80f;
        persona3.puesto = "Ordeñar";
        persona3.sueldo = persona3.ajustaSueldoA(persona2);
               
         
        System.out.println(persona1.imprimePersona());
        System.out.println(persona2.imprimePersona());
        System.out.println(persona3.imprimePersona());
        
        
     System.out.println(persona1.cuantoGana()+"€");
     System.out.println(persona2.cuantoGana()+"€");
     System.out.println(persona3.cuantoGana()+"€");
     
     Vaca v1=new Vaca();
     v1.nombre="Vaca 1";
     v1.peso=250;
     v1.litros=new float[1];
     v1.asignarFuncion();
     
     
     Vaca v2=new Vaca();
     v2.nombre="Vaca 2";
     v2.peso=150;
     v2.litros=new float[1];
     v2.asignarFuncion();
     
     Vaca[] todas={v1,v2};
     
     persona3.quedarmeVacas( todas);
     persona3.imprimeVaca(todas);
     
        System.out.println("La propocion del sueldo es " +persona1.proporcionSueldo(persona2.sueldo)  );
     
     
       
        
        
        
        
 
     
    }}
    

