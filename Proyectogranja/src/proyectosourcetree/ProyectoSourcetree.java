package proyectosourcetree;

import java.util.Scanner;

public class ProyectoSourcetree {

    public static void main(String[] args) {
        //Pedir por teclado el nº de filas y el nº de columnas
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduce el  numero de filas ");
        int nFilas = sc.nextInt();
        System.out.println("Introduce el  numero de columnas ");
        int nColumnas = sc.nextInt();
        int[][] matriz = new int[nFilas][nColumnas];
        //llama a las tres funciones e imprima el resultado de las 3  
        System.out.println("----------------");
        rellenar(matriz);
        System.out.println("----------------");
        rellenarMult3(matriz);
        System.out.println("----------------");
        rellenarAlReves(matriz);
     

    }

    public static void rellenar(int[][] matriz) {
        //Rellene la matriz con numeros consecutivos. Por ejemplo:
        //Si la matriz es de 3x4:
        // 0 1 2 3
        // 4 5 6 7 
        // 8 9 10 11
      
        for (int i=0; i<matriz.length;i++){
            for(int j=0; j<matriz[i].length;j++){
            matriz[i][j]=j+i*matriz[i].length;
            
             
            }
            
                  }
        System.out.println(imprimeMatriz(matriz));
      

    }

    public static void rellenarMult3(int[][] matriz) {
        //Rellene la matriz con multiplos de tres.
        //Ej con matriz 3x4:
        // 0 3 6 9
        // 12 15 18 21
        // 24 27 30 33
      
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {

                matriz[i][j] = (j+i*matriz[i].length) * 3;
            }
        }
        System.out.println(imprimeMatriz(matriz));
    }

    public static void rellenarAlReves(int[][] matriz) {
        //Rellene la matriz con numeros consecutivos a la inversa.
        // 11 10 9 8
        // 7 6 5 4
        // 3 2 1 0
        
        for (int i = matriz.length-1; i >= 0; i--) {
            for (int j = matriz[i].length-1; j >= 0; j--) {
             matriz[matriz.length-i-1][matriz[i].length-j-1]=j+i*matriz[i].length;
            
               
                
            }
           
        }
        System.out.println(imprimeMatriz(matriz));
             
    }
    
    public static String imprimeMatriz(int[][] mat){
        String ret="";
        for(int i=0;i<mat.length;i++){
            for(int j=0;j<mat[i].length;j++){
                ret+=mat[i][j]+" ";
            }
            ret+="\n";
        }
        
        return ret;
}
            
}


    
