
package clases;


public class Persona {
    String nombre;
    String apellidos;
    String nacionalidad;
    float altura;
    int edad;
    String puesto;
    float sueldo;
    
    
    public String imprimePersona(){
        return nombre+" "+apellidos+" "+nacionalidad+" "
                +altura+" "+edad;
    }
    /**
     * 
     * @param p2 persona con la que se compara
     * @return true si this es mayor, false si this es minor o de la misma edad
     */
    public boolean esMasViejaQue(Persona p2){
    return edad>p2.edad;    
    }
    public String cuantoGana(){
        return nombre+" gana"+sueldo+" €";
    }
    public float proporcionSueldo(Persona p2){
        float proporcion=p2.sueldo/sueldo;
        return proporcion;
    }
    public void ajustaSueldoA(Persona p2){
        if(puesto.equals("carniceria") && p2.puesto.equals("gerencia")){
            sueldo=p2.sueldo*0.75f;
        }
        if(puesto.equals("carniceria") && p2.puesto.equals("ordeñador")){
            sueldo=p2.sueldo*0.5f;
        }
    }
}
