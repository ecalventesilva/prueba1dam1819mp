/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author 1DAM
 */
public class Clases {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Persona persona1 = new Persona();
        persona1.nombre= "Adolfo";
        persona1.apellidos="Lopez";
        persona1.nacionalidad="Española";
        persona1.edad=20;
        persona1.altura=1.75f;
        
        Persona persona2 = new Persona();
        persona2.nombre= "Lola";
        persona2.apellidos= "Mento";
        persona2.edad =25;
        persona2.nacionalidad= "Canadiense";
        persona2.altura= 1.69f;
                
        
        System.out.println(persona1.imprimePersona());
        System.out.println(persona2.imprimePersona());
        System.out.println("¿Es Adolfo mayor que Lola?");
        System.out.println(persona1.esMasViejaQue(persona2));
        System.out.println("¿Y al revés?");
        System.out.println(persona2.esMasViejaQue(persona1));
    }
    
}
